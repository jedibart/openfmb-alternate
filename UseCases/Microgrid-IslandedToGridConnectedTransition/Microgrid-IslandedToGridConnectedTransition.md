# Microgrid - Islanded to Grid Connected Transition

## Scenario 1 - Grid Restoration Causes Reconnect

This use case deals with the resynchronization and reconnection transition behavior from islanded
mode to grid-connected mode of the microgrid.
In this scenario, power is restored to the grid and is detected by the island recloser (or switch)
at the point of common coupling (PCC); this starts the resynchronization / reconnection
(synch-check) activity, only if the DMS provides a confirmation status to the Optimizer of the
restored power grid and also granting permission to the island recloser by removing its control
block. The balancing of the grid side and island side voltage and frequency are managed by the
optimizer in conjunction with the battery inverters. Once the recloser synch-check function
criteria is met, the Microgrid is resynchronized and reconnected to the grid. Immediately, the
Optimizer messages the battery inverters to switch from voltage-source “Sv” to current-source “Sc”
mode.  Additionally, the microgrid optimizer and the Utility SCADA receive the recloser close
status to update their models.

There is one scenarios to this use case: Grid Power Restored .

Grid Power Restored

Several steps are followed:

1. Island recloser detects return of power to the grid at PCC
    and publishes readings and status to Optimizer and DMS.
2. DMS sends confirmation status to Optimizer
    and also sends remove control block command to island recloser.
3. Optimizer receives readings and status of the island recloser
    and the confirmation status from DMS
    and begins the grid resynchronization and reconnection process
4. Optimizer publishes the synch-check command to the Island recloser
    and receives periodic statuses and readings from the solar inverter,
    batter inverter, and meters.
5. Island recloser receives the synch-check command and initiates the resynch process
6. Optimizer manages all battery inverters to match grid-side voltage and frequency
    by publishing desired setpoints
7. Battery inverters publish readings and status to the Optimizer
8. Optimizer receives Battery inverter readings and status and adjusts setpoints to match grid
9. Island recloser resynchs and publishes readings and status to the Optimizer and Utility SCADA
10. Optimizer receives Island recloser status message
11. Optimizer publishes battery inverter change setting to current-source “Sc” mode
12. Battery iniverter receives command and switches to current-source “Sc” mode
13. Utility SCADA receives island recloser status message

The Microgrid Island to Grid-Connected Transition performs the following functions:

1. Trigger Battery Inverter to switch to current-source “Sc” mode
2. Notify microgrid optimizer and DMS of status
3. Controls battery inverter settings to balance voltage and frequency of island to grid 
4. Ensure  DMS provides permission 
5. Optimizer activates recloser synch-check function 

### Scenario 1 Diagram

```plantuml
@startuml
participant "Battery Inverter:\nMeter" as BttryInvMtr
participant "Solar Inverter:\nMeter" as SlrInvMtr
participant "Solar Inverter" as SlrInv
participant "Recloser"
participant "Microgrid Optimizer" as McrgrdOpt
participant "Battery Inverter:\nEnergy Storage System" as BttryInvEss
participant "DMS"
participant "SCADA"

"Recloser" -> McrgrdOpt : Recloser Readings
note left
10. Island Recloser senses
the return of the potential
from the grid and sends readings
(voltage & frequency)
to Optimizer.
Note its status is still open, no change.
end note

Recloser -> DMS : Recloser Readings
note right
300. Utility DMS receives
Recloser readings,
confirms power is restored.
end note
activate DMS

DMS -> McrgrdOpt
note right
300b. Informs Microgrid Optimizer
that it isready for the microgrid
to be reconnected
end note
activate McrgrdOpt

McrgrdOpt -> Recloser : Grid Ready Message
note left
30. Optimizer sends a sync command
to Island Recloser to sync.
Note the Island Recloser won't be
closed until both sides sync-ed up
(same V/Hz).
end note

activate Recloser
note right
40. Island Recloser receives sync command
from the Optimizer and starts monitoring
V/Hz at both side (grid & DG line).
It will close its switch when both
sides are balanced.
end note

DMS -> Recloser : Remove Block
note right
350. Utility DMS sends command
to Recloser to remove control block.
end note
deactivate DMS

BttryInvMtr -> McrgrdOpt : Meter Readings
note right
Optimizer receives status and readings (V & Hz)
from battery inverter and Solar inverter
end note
SlrInvMtr -> McrgrdOpt : Meter Readings
BttryInvEss -> McrgrdOpt : Status
BttryInvEss -> McrgrdOpt : Meter Readings (V & Hz)
SlrInv -> McrgrdOpt : Status
SlrInv -> McrgrdOpt : Meter Readings

McrgrdOpt -> BttryInvEss : Battery Inverter Control
note right
80. Optimizer sends V/Hz set points
to battery inverter to match based
on the grid readings.
end note
activate BttryInvEss
note left
100. Battery Inverter adjusts set points
to match up with what asked by the Optimizer.
end note

Recloser -> Recloser : Close Breaker
note left
110. Island Recloser monitoring V/Hz on both
sides and automatically closes if both sides sync-ed up.
end note

Recloser -> SCADA : Island Recloser Status
Recloser -> McrgrdOpt : Island Recloser Status
deactivate Recloser

McrgrdOpt -> BttryInvEss
note right
140. Optimizer sends command to the battery inverter
to change its setting from Sv to Sc.
end note
deactivate McrgrdOpt

BttryInvEss -> BttryInvEss: Switch from Sv to Sc
deactivate BttryInvEss
@enduml
```